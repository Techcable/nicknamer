/*
 * Copyright 2015 Marvin Sch�fer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.nickname;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.MetricsLite;

/**
 *
 * � Copyright 2015 inventivetalent
 *
 * @author inventivetalent
 *
 */
public class NickNamer extends JavaPlugin implements Listener {

	public static NickNamer			instance;
	public static String			prefix					= "�6[NickNamer]�r ";

	private static PacketListener	packetListener;

	public static long				API_TIMEOUT				= 120000;
	public static boolean			NICK_TAB				= true;
	public static boolean			NICK_CHAT				= true;
	public static boolean			NICK_SCOREBOARD			= true;

	public static boolean			QUICK_COMMAND			= true;
	public static boolean			DISABLE_EXISTING		= false;
	public static List<String>		RANDOM_NAMES;

	public static String			MSG_PERM_GENERAL		= "�cNo permission.";
	public static String			MSG_PERM_NAME			= "�cYou don't have permission to use that name!";
	public static String			MSG_PERM_SKIN			= "�cYou don't have permission to use that skin!";
	public static String			MSG_PERM_CHANGE_OTHER	= "�cYou don't have permission to change other's nicknames.";
	public static String			MSG_NAME_INVALID		= "�cInvalid nick name!";
	public static String			MSG_NAME_LENGTH			= "�cThat name is too long!";
	public static String			MSG_NAME_CHANGED		= "�aChanged �b%player%'s �aname to �b%name%�a.";
	public static String			MSG_NAME_TAKEN			= "�cThis name is already taken!";
	public static String			MSG_SKIN_CHANGED		= "�aChanged �b%player%'s �askin to �b%skin%'s�a skin.";
	public static String			MSG_SKIN_UPDATE			= "�aIt will be updated in �b%time%�a minute(s).";
	public static String			MSG_CLEARED				= "�aCleared �b%player%'s �anick name and skin.";
	public static String			MSG_PLAYER_NOT_ONLINE	= "�cThat player is not online!";

	@Override
	public void onEnable() {
		instance = this;
		Bukkit.getPluginManager().registerEvents(this, this);

		this.saveDefaultConfig();
		prefix = this.getConfig().getString("message.prefix", prefix).replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
		API_TIMEOUT = this.getConfig().getLong("apiTimeout", API_TIMEOUT);
		NICK_TAB = this.getConfig().getBoolean("nick.tab", NICK_TAB);
		NICK_CHAT = this.getConfig().getBoolean("nick.chat", NICK_CHAT);
		NICK_SCOREBOARD = this.getConfig().getBoolean("nick.scoreboard", NICK_SCOREBOARD);

		QUICK_COMMAND = this.getConfig().getBoolean("quickCommand", QUICK_COMMAND);
		DISABLE_EXISTING = this.getConfig().getBoolean("disableExisting", DISABLE_EXISTING);
		RANDOM_NAMES = new ArrayList<>();
		for (String s : this.getConfig().getStringList("randomNames")) {
			RANDOM_NAMES.add(s.replaceAll("(&([a-fk-or0-9]))", "\u00A7$2"));
		}

		MSG_PERM_GENERAL = this.getConfig().getString("message.permission.general", MSG_PERM_GENERAL).replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
		MSG_PERM_NAME = this.getConfig().getString("message.permission.name", MSG_PERM_NAME).replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
		MSG_PERM_SKIN = this.getConfig().getString("message.permission.skin", MSG_PERM_SKIN).replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
		MSG_PERM_CHANGE_OTHER = this.getConfig().getString("message.permission.change_other", MSG_PERM_CHANGE_OTHER).replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
		MSG_NAME_INVALID = this.getConfig().getString("message.name.invalid", MSG_NAME_INVALID).replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
		MSG_NAME_LENGTH = this.getConfig().getString("message.name.length", MSG_NAME_LENGTH).replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
		MSG_NAME_CHANGED = this.getConfig().getString("message.name.changed", MSG_NAME_CHANGED).replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
		MSG_NAME_TAKEN = this.getConfig().getString("message.name.taken", MSG_NAME_TAKEN).replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
		MSG_SKIN_CHANGED = this.getConfig().getString("message.skin.changed", MSG_SKIN_CHANGED).replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
		MSG_SKIN_UPDATE = this.getConfig().getString("message.skin.update", MSG_SKIN_UPDATE).replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
		MSG_CLEARED = this.getConfig().getString("message.cleared").replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
		MSG_PLAYER_NOT_ONLINE = this.getConfig().getString("message.player.not_online", MSG_PLAYER_NOT_ONLINE).replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");

		packetListener = new PacketListener(this);

		System.out.println("[NickNamer] Starting SkinLoader.");
		if (SkinLoader.instance != null) {
			SkinLoader.instance.active = false;
			SkinLoader.instance = null;
		}
		new SkinLoader().start();

		if (!Bukkit.getOnlineMode()) {
			System.out.println("[NickNamer] Server is in offline mode. Starting UUIDResolver.");
			if (UUIDResolver.instance != null) {
				UUIDResolver.instance.active = false;
				UUIDResolver.instance = null;
			}
			new UUIDResolver().start();
		}

		try {
			MetricsLite metrics = new MetricsLite(this);
			if (metrics.start()) {
				System.out.println("[NickNamer] Metrics started.");
			}
		} catch (Exception e) {
		}
	}

	@Override
	public void onDisable() {
		if (packetListener != null) {
			packetListener.disable();
		}
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		if (Nicks.isNicked(p.getUniqueId())) {
			String nick = Nicks.getNick(p.getUniqueId());
			if (NickNamer.NICK_CHAT) {
				p.setDisplayName(nick);
			}
			if (NickNamer.NICK_TAB) {
				p.setPlayerListName(nick);
			}
		}
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (label.equalsIgnoreCase("nickname") || label.equalsIgnoreCase("nicknamer") || label.equalsIgnoreCase("nick") || label.equalsIgnoreCase("setnick") || label.equalsIgnoreCase("changenick")) {
			if (args.length == 0) {
				if (QUICK_COMMAND) {
					if (sender instanceof Player) {
						((Player) sender).chat("/nick " + sender.getName() + " random");
						return true;
					}
				}
			}
			if (args.length >= 2) {
				if (args.length == 2 && !sender.hasPermission("nick.command.name") || args.length == 3 && !sender.hasPermission("nick.command.skin")) {
					sender.sendMessage(prefix + MSG_PERM_GENERAL);
					return false;
				}
				Player target = Bukkit.getPlayerExact(args[0]);
				if (target == null || !target.isOnline()) {
					sender.sendMessage(prefix + MSG_PLAYER_NOT_ONLINE);
					return false;
				}
				if (sender instanceof Player && !((Player) sender).getUniqueId().equals(target.getUniqueId())) {
					if (!sender.hasPermission("nick.other")) {
						sender.sendMessage(prefix + MSG_PERM_CHANGE_OTHER);
						return false;
					}
				}
				String nick = args[1].replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
				if (nick.equalsIgnoreCase("clear")) {
					if (!sender.hasPermission("nick.command.clear")) {
						sender.sendMessage(prefix + MSG_PERM_GENERAL);
						return false;
					}
					Nicks.removeSkin(target.getUniqueId());
					Nicks.removeNick(target.getUniqueId());
					sender.sendMessage(prefix + MSG_CLEARED.replace("%player%", target.getName()));
					return true;
				}
				if (nick.equalsIgnoreCase("random")) {
					if (RANDOM_NAMES.isEmpty()) {
						sender.sendMessage(prefix + "�cNo random names configured!");
						return false;
					} else {
						String random = RANDOM_NAMES.get(new Random().nextInt(RANDOM_NAMES.size()));
						nick = random;
					}
				}
				if (nick == null || nick.isEmpty()) {
					sender.sendMessage(prefix + MSG_NAME_INVALID);
					return false;
				}
				if (nick.length() > 15) {
					sender.sendMessage(prefix + MSG_NAME_LENGTH);
					return false;
				}

				if (DISABLE_EXISTING) {
					boolean exists = false;
					for (Player p : Bukkit.getOnlinePlayers()) {
						if (nick.equals(p.getName())) {
							exists = true;
							break;
						}
					}
					if (!exists) {
						for (OfflinePlayer op : Bukkit.getOfflinePlayers()) {
							if (op != null && nick.equals(op.getName())) {
								exists = true;
								break;
							}
						}
					}
					if (exists) {
						sender.sendMessage(prefix + MSG_NAME_TAKEN);
						return false;
					}
				}

				String skin = null;
				if (args.length == 3) {
					skin = args[2];
				}
				if (!sender.hasPermission("nick.name." + nick)) {
					sender.sendMessage(prefix + MSG_PERM_NAME);
					return false;
				}
				try {
					Nicks.setNick(target.getUniqueId(), nick);
				} catch (Exception e) {
					sender.sendMessage(prefix + "�cException while changing name: �7" + e.getMessage());
					e.printStackTrace();
					return false;
				}
				sender.sendMessage(prefix + MSG_NAME_CHANGED.replace("%player%", target.getName()).replace("%name%", nick));
				if (skin != null) {
					if (!sender.hasPermission("nick.skin." + skin)) {
						sender.sendMessage(prefix + MSG_PERM_SKIN);
						return false;
					}
					long delay = Nicks.setSkin(target.getUniqueId(), skin);
					sender.sendMessage(prefix + MSG_SKIN_CHANGED.replace("%player%", target.getName()).replace("%skin%", skin));
					if (delay > 0) {
						sender.sendMessage(prefix + MSG_SKIN_UPDATE.replace("%time%", new SimpleDateFormat("mm:ss").format(new Date(delay))));
					}
				}
				return true;
			} else {
				sender.sendMessage(prefix + "�c/nick <player> <nick|clear> [skin]");
				return false;
			}
		}
		return false;
	}

}
