/*
 * Copyright 2015 Marvin Sch�fer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.nickname;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.plugin.Plugin;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import de.inventivegames.packetlistener.handler.PacketHandler;
import de.inventivegames.packetlistener.handler.PacketOptions;
import de.inventivegames.packetlistener.handler.ReceivedPacket;
import de.inventivegames.packetlistener.handler.SentPacket;

/**
 *
 * � Copyright 2015 inventivetalent
 *
 * @author inventivetalent
 *
 */
public class PacketListener {

	private static int				version	= 170;

	private static Class<?>			classGameProfile;
	private static Class<?>			classProperty;
	private static Class<?>			classPropertyMap;

	static {
		try {
			if (Reflection.getVersion().contains("1_7")) {
				version = 170;
			}
			if (Reflection.getVersion().contains("1_8")) {
				version = 180;
			}
			if (Reflection.getVersion().contains("1_8_R1")) {
				version = 181;
			}
			if (Reflection.getVersion().contains("1_8_R2")) {
				version = 182;
			}

			if (version < 180) {
				classGameProfile = Class.forName("net.minecraft.util.com.mojang.authlib.GameProfile");
				classProperty = Class.forName("net.minecraft.util.com.mojang.authlib.properties.Property");
				classPropertyMap = Class.forName("net.minecraft.util.com.mojang.authlib.properties.PropertyMap");
			} else {
				classGameProfile = Class.forName("com.mojang.authlib.GameProfile");
				classProperty = Class.forName("com.mojang.authlib.properties.Property");
				classPropertyMap = Class.forName("com.mojang.authlib.properties.PropertyMap");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static PacketHandler	handler;

	public PacketListener(Plugin pl) {
		PacketHandler.addHandler(handler = new PacketHandler(pl) {

			@Override
			@PacketOptions(forcePlayer = true)
			public void onSend(SentPacket packet) {
				if (packet.getPacketName().equals("PacketPlayOutNamedEntitySpawn") && version < 180) {
					if (packet.hasPlayer()) {
						try {
							Object profile = packet.getPacketValue("b");
							if (profile != null) {
								profile = disguiseProfile(profile);
								packet.setPacketValue("b", profile);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
				if (packet.getPacketName().equals("PacketPlayOutPlayerInfo")) {
					if (packet.hasPlayer()) {
						if (version < 180 && (int) packet.getPacketValue("action") == 4) return;// Cancel here if the player is currently being removed
						try {
							Object profile = packet.getPacketValue(version < 180 ? "player" : "b");
							if (version < 180) {
								if (profile != null) {
									profile = disguiseProfile(profile);
									packet.setPacketValue("player", profile);
								}
								if (profile != null) {
									packet.setPacketValue("username", AccessUtil.setAccessible(profile.getClass().getDeclaredField("name")).get(profile));
								}
							} else {// PlayerInfoData handling
								List list = new ArrayList<>((List) profile);
								for (Object obj : list) {
									Field profileField = AccessUtil.setAccessible(obj.getClass().getDeclaredField("d"));
									profile = disguiseProfile(profileField.get(obj));
									profileField.set(obj, profile);
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}

			@Override
			public void onReceive(ReceivedPacket packet) {
			}
		});
	}

	private static Object disguiseProfile(final Object profile) throws Exception {
		Field idField = AccessUtil.setAccessible(profile.getClass().getDeclaredField("id"));
		Field nameField = AccessUtil.setAccessible(profile.getClass().getDeclaredField("name"));
		Field propertyMapField = AccessUtil.setAccessible(profile.getClass().getDeclaredField("properties"));

		final UUID id = (UUID) idField.get(profile);
		final String name = (String) nameField.get(profile);
		final Object propertyMap = propertyMapField.get(profile);

		if (!Nicks.isNicked(id) && !Nicks.hasSkin(id)) return profile;// return the profile as it is if nothing has to be changed

		final Object profileClone = classGameProfile.getConstructor(UUID.class, String.class).newInstance(id, name);// Create a clone of the profile since the server's PlayerList will use the original
																													// profiles
		Object propertyMapClone = propertyMap;

		if (Nicks.hasSkin(id)) {
			JSONObject skinData = SkinLoader.getSkin(Nicks.getSkin(id));
			if (skinData != null) {
				if (skinData.containsKey("properties")) {
					skinData = (JSONObject) ((JSONArray) skinData.get("properties")).get(0);

					if (skinData != null) {
						classPropertyMap.getMethod("clear").invoke(propertyMapClone);
						classPropertyMap.getSuperclass().getMethod("put", Object.class, Object.class).invoke(propertyMapClone, "textures", classProperty.getConstructor(String.class, String.class, String.class).newInstance("textures", skinData.get("value"), skinData.get("signature")));
					}
				}
			}
		}

		propertyMapField.set(profileClone, propertyMapClone);

		String nick = name;
		if (Nicks.isNicked(id)) {
			nick = Nicks.getNick(id);
		}
		nameField.set(profileClone, nick);
		return profileClone;
	}

	public void disable() {
		PacketHandler.removeHandler(handler);
	}

}
