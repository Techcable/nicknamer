/*
 * Copyright 2015 Marvin Sch�fer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.nickname;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

/**
 * @author inventivetalent
 *
 */
public class Nicks {

	private static final Map<UUID, String>	nickedPlayers	= new HashMap<>();
	private static final Map<UUID, String>	storedNames		= new HashMap<>();
	protected static final Map<UUID, UUID>	skins			= new HashMap<>();

	/**
	 * @param id
	 *            {@link UUID} of the player
	 * @return <code>true</code> if the player has a nickname
	 */
	public static boolean isNicked(UUID id) {
		return nickedPlayers.containsKey(id);
	}

	/**
	 * @param nick
	 *            nickname
	 * @return <code>true</code> if the nickname is used by a player
	 */
	public static boolean isNickUsed(String nick) {
		return nickedPlayers.containsValue(nick);
	}

	/**
	 * @param id
	 *            {@link UUID} of the player
	 * @return nickname of the player
	 */
	public static String getNick(UUID id) {
		return nickedPlayers.get(id);
	}

	/**
	 * @param id
	 *            {@link UUID} of the player
	 * @param nick
	 *            nickname to set
	 */
	public static void setNick(UUID id, String nick) {
		if (isNicked(id)) {
			removeNick(id);
		}
		nickedPlayers.put(id, nick);

		Player p = Bukkit.getPlayer(id);
		if (p != null) {
			storedNames.put(id, p.getDisplayName());
			if (NickNamer.NICK_CHAT) {
				p.setDisplayName(nick);
			}
			if (NickNamer.NICK_TAB) {
				p.setPlayerListName(nick);
			}
			if (NickNamer.NICK_SCOREBOARD) {
				Scoreboard sb = p.getScoreboard();
				if (sb == null) {
					sb = Bukkit.getScoreboardManager().getMainScoreboard();
				}
				if (sb != null) {
					Team t = sb.getPlayerTeam(p);
					if (t != null) {
						t.removePlayer(p);
						t.addEntry(nick);
					}
				}
			}
		}

		updatePlayer(id);
	}

	/**
	 * @param id
	 *            {@link UUID} of the player
	 */
	public static void removeNick(UUID id) {
		String nick = nickedPlayers.remove(id);

		Player p = Bukkit.getPlayer(id);
		if (p != null) {
			if (NickNamer.NICK_CHAT) {
				p.setDisplayName(storedNames.get(id));
			}
			if (NickNamer.NICK_TAB) {
				p.setPlayerListName(storedNames.get(id));
			}
			if (NickNamer.NICK_SCOREBOARD) {
				Scoreboard sb = p.getScoreboard();
				if (sb == null) {
					sb = Bukkit.getScoreboardManager().getMainScoreboard();
				}
				if (sb != null) {
					Team t = null;
					for (Team tm : sb.getTeams()) {
						for (String s : tm.getEntries()) {
							if (s.equals(nick)) {
								t = tm;
								break;
							}
						}
					}
					if (t != null) {
						t.removeEntry(nick);
						t.addPlayer(p);
					}
				}
			}
		}
		storedNames.remove(id);

		updatePlayer(id);
	}

	/**
	 * @param nick
	 *            nickname
	 * @return {@link List} of players with the nickname
	 */
	public List<UUID> getPlayersWithNick(String nick) {
		List<UUID> list = new ArrayList<>();
		for (Entry<UUID, String> entry : nickedPlayers.entrySet()) {
			if (entry.getValue().equals(nick)) {
				list.add(entry.getKey());
			}
		}
		return list;
	}

	/**
	 * @param id
	 *            {@link UUID} of the player
	 * @param skinOwner
	 *            name of the skin owner
	 * @return time until the skin will be fully loaded
	 */
	public static long setSkin(UUID id, String skinOwner) {
		if (hasSkin(id)) {
			removeSkin(id);
		}

		@SuppressWarnings("deprecation")
		UUID skinID = Bukkit.getOfflinePlayer(skinOwner).getUniqueId();

		skins.put(id, skinID);

		if (!Bukkit.getOnlineMode()) {
			UUIDResolver.resolve(skinOwner);
		} else {
			long l = SkinLoader.load(skinOwner);
			if (l < 0) {
				updatePlayer(id);
			}
			return l;
		}
		return -1L;
	}

	/**
	 * @param id
	 *            {@link UUID} of the player
	 */
	public static void removeSkin(UUID id) {
		skins.remove(id);

		updatePlayer(id);
	}

	/**
	 * @param id
	 *            {@link UUID} of the player
	 * @return {@link UUID} of the skin owner
	 */
	public static UUID getSkin(UUID id) {
		if (hasSkin(id)) return skins.get(id);
		return id;
	}

	/**
	 * @param id
	 *            {@link UUID} of the player
	 * @return <code>true</code> if the player has a skin
	 */
	public static boolean hasSkin(UUID id) {
		return skins.containsKey(id);
	}

	/**
	 * @param id
	 *            {@link UUID} of the skin owner
	 * @return {@link List} of players with the skin
	 */
	public static List<UUID> getPlayersWithSkin(UUID id) {
		List<UUID> list = new ArrayList<>();
		for (Entry<UUID, UUID> entry : skins.entrySet()) {
			if (entry.getValue().equals(id)) {
				list.add(entry.getKey());
			}
		}
		return list;
	}

	/**
	 * Updates the player's name and skin to all online players
	 *
	 * @param id
	 *            {@link UUID} of the player
	 */
	public static void updatePlayer(UUID id) {
		Player p = Bukkit.getPlayer(id);
		updatePlayer(p);
	}

	/**
	 * Updates the player's name and skin to all online players
	 *
	 * @param p
	 *            {@link Player}
	 */
	public static void updatePlayer(final Player p) {
		if (p == null || !p.isOnline()) return;
		Bukkit.getScheduler().scheduleSyncDelayedTask(NickNamer.instance, new Runnable() {

			@Override
			public void run() {
				List<Player> canSee = new ArrayList<>();
				for (Player p1 : Bukkit.getOnlinePlayers()) {
					if (p1.canSee(p)) {
						canSee.add(p1);
						p1.hidePlayer(p);
					}
				}
				for (Player p1 : canSee) {
					p1.showPlayer(p);
				}
			}
		});
	}

}
